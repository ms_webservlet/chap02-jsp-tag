package com.mseop.jspTag;

public class Application {
	/*
	 * JSP Action Tag
		: xml 기술 이용해서 jsp문법을 확장시켜서 추가하는 태그 
		
		1. 표준 Action Tag
		 <jsp:include />  :현재페이지에 특정 페이지를 포함
		 <jsp:forward /> :  
		 <jsp:param />   : 필요한값을 더 담아서 보낸다 ( 파라미터 같이 )
		 <jsp:usebean /> : dto,dao,entity와 동일하다.
		 <jsp:setProperty /> :
		 <jsp:getProperty /> :
		 
		1 <%@ include file="common.jsp" %>
		     common.jsp 만들어서 이동 "안녕하세요 common페이지입니다."
		     해서 실행하면 cmmon.jsp파일에 쓴 글이 나오게된다.
		
		1-2 <jsp:include page="common.jsp" %>
		     실행결과 1랑 똑같이  실행이 되지만. 실행순서가 달라진다. 
		     원래 같은 변수 이름은 사용못하지만 사용이가능하다.
		     더 확장이 되었다.
		
		2 <jsp:forward />
		   <% request.setAttribute("name", "홍길동") %>
		   <jsp:forward page = "testForward.jsp"/> 
		   이런 정보를 넘겨주겠다.
		
		2-2 testForward.jsp
		    <h1> <% request.getAttribute("name") %> 님환영합니다. </h1>
		
		     - 기존에 썻떤 1번은 무시하고 바로 Forward를 시켜버린다. - 2번만보인다.
		
		
		---------------------------------------------------------------------------------------
		
		
		2. EL & JSTL 
	     문법 : ${ value }
	     예시 : <%= request.getParameter("name") %>
	              ${ param.name 
	
	     3-1 서블릿페이지
	     request.setAttribute("name", "홍길동");
	     RequestDispatcher rd = request.getRequestDispatcher("경로.jsp");
	     rd.forward(request, response);
	
	     3-2 경로.jsp 페이지
	     (기존) <% String name = (String) request.getAttribute("name") %>
	             <%= name %>
	
	     (EL)  ${ requestScope.name }
	     이한줄로 한번에 해결할 수 있다.
	     
	     - 반복문 주의사항 - 
	     (기존) <% for(int i=0; i<items.size(); i++){ %>
			
		 <%= i %> : <%= items.get(i) %><br>
	             <% } %>
	     
	      (EL) %{ requestScope.items[0]}
	           %{ requestScope.items[1]}
	           %{ requestScope.items[2]}


	     - 객체 넘기기 -
	     MemberDTO member = new MemberDTO("홍길동", 20, "010-1234-5678", "hong@ohgiraffers.com");
			
	     request.setAttribute("member", member);
			
	     RequestDispatcher rd = request.getRequestDispatcher("views/el/testEl2.jsp");
	     rd.forward(request, response);
	
	
	     - testEl2.jsp -
	     (기존)
	     <% MemberDTO member = (MemberDTO) request.getAttribute("member"); %>
		
	     이름 : <%= member.getName() %> <br>
	     나이 : <%= member.getAge() %> <br>
	     전화번호 : <%= member.getPhone() %> <br>
	     이메일 : <%= member.getEmail() %> <br>
	
	     (EL) ${ requestScope.member.name}
	          ${ requestScope.member.age}
	
	
	**************************************************************************
	     <%= request.getParameter("name") %>
	     <%= name %>
	     > (el) ${ requestScope.name }
	
	     반복문[배열 가져오기]
	     ${ requestScope.items[0]}
	     & ${ items[0] }
	
	     객체 꺼내오기
	     ${ requestScope.변수이름.name  }
	     & ${ 변수이름.name }
	
	     URL로 전송한 값 꺼내오기
	     ${ param.name }
	     * URL 전송은 param을 꼭 넣어야 한다. 
	     여러개일시
	     ${ paramValues.no["0"] } 와 ${ paramValues.no["1"] }
	     삼항연산자 넣기
	     ${ (empty param.option)? "옵션없음" : param.option }
	
	**************************************************************************
	
	
	
	
	---------------------------------------------------------------------------------------
	
	3. 커스텀 액션 태그 (별로 라이브러리추가후 사용) 접두어.
	     <c:set var="cnt" />
	
	
	
	
	
	
	
	
		 * */
	
	}
