package com.mseop.jsp;

public class Application {

	/*
	 * 2022-03-06
		JSP
		: html 안에 java코드를 넣는다.
		
		<%@ 지시자태그 %>	: 페이지에 대한 설정을 지시자 태그이다.
		<%! 선언 태그 %>		: 변수,자바코드가 들어간다.
		<%-- 주석 태그--%> 	: 주석
		<% 스크립트릿 태크%>	: 우리의 자바코드는 여기에
		<%= JSP값 표현 태그 %> 	: 스크립트릿 에쓴 변수를 여기에다가 사용한다.
		
		JSP는 translate -> compile -> instance생성 -> run이 되는 순서이다.
		META_INF 파일에 context 복붙하기
		translate 되는거 보여주기.
		<Context workDir="절대경로/translate">
		
		 -선언태그-
		<%!  
		     private String name;
		     private int age;
		%>
		
		 -스크립트릿태그-
		<%
		     name = "홍길동";
		     System.out.println(name);
		%>
		// Console창 name : 홍길동
		
			                   PrintWrite pw = new PrintWrite
		name : <%= name  %>   ==  pw.print(name);
		
		
		
		
		- 지시자 태그 에서 사용 가능한 속성들 -
		
		- pageDirective -
		errorPage
		isErrorPage
		import
		page 지시자 태그
		include 지시자 태그
		taglib 태그
		
		<%@ page language="java" contentType="text/html; charset=UTF-8"
		    pageEncoding="UTF-8" import="java.util.Date" errorPage="errorPage.jsp"%>
		
		import 지시자태그에 달아준다.
		errorPage 에러발생할 페이지를 jsp를 따로만들어서 경로를 잡아준다.
		
		[에러페이지] errorPage.jsp로 이동해 지시자태그에서 
		isErrorPage="true"%> 를 써주면 된다. 기본값은 false다.
		
		errorPage.jsp
		<%
		     String exceptionType = exception.getClass().getName();
		     String exceptionMessage = exception.getMessage();
		%>
		에러페이지를 발생시키기 위한 코드.
		
		
		
		- includeDriective.jsp
		<%@ include file="today.jsp"%>
		
		include지시자 태그를 이용하게 되고 file속성에 jsp경로를 지정해주면
		해당 jsp에 작성한 내용을 그대로 현재 jsp파일을 동작하시게 된다.
		
		다른.jsp
		<%@ page import="java.util.Date, java.text.SimpleDateFormat"%>
		날짜 import 
		<%
		    Date today = new Date();
		    SimpleDateFormat sdf = new SimpleDateFormat("yyyy년 MM월 dd일 E요일 a hh시 mm분 ss초"); 
		SimpleDateFormat : 어떤 표현을 나타낼건지 ?
		%>
	 * */
}
